package CS;

public class Player {
	
	private String name;
	private int hp;
	Gun gun;
	public Player(String name, int hp, Gun gun) {
		super();
		this.name = name;
		this.hp = hp;
		this.gun = gun;
	}
	
	public void hurt(Gun g)
	{
	    this.setHp(this.getHp()-g.getDamage());
	    if(this.getHp()<=0)
	    {
	    	System.out.println(this.getName()+"挂了");
	    	System.exit(1);
	    }else
	    {
	    	System.out.println(this.getName()+"受伤了，当前血量："+this.getHp());
        }
	}
	
	public void fire(Player p)
	{
		if(this.getGun()==null)
		{
			System.out.println(this.getName()+"没有枪！！！");
			return;
		}else if(this.getGun().getBullet_count()==0)
		{
			System.out.println("枪没有子弹了，自动填充中。。。");
			this.getGun().add_bullets(3);
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("子弹填充完毕！");
		}
		System.out.println(this.getName()+"开枪了！！#￥%￥%……@");
		this.getGun().shoot(p);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getHp() {
		return hp;
	}

	public void setHp(int hp) {
		this.hp = hp;
	}

	public Gun getGun() {
		return gun;
	}

	public void setGun(Gun gun) {
		this.gun = gun;
	}
	
	

}
