package CS;

public class Gun {
	
	private String model;
	private int damage;
	private int bullet_count=0;
	
	public Gun(String model, int damage, int bullet_count) {
		super();
		this.model = model;
		this.damage = damage;
		this.bullet_count = bullet_count;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public int getDamage() {
		return damage;
	}

	public void setDamage(int damage) {
		this.damage = damage;
	}

	public int getBullet_count() {
		return bullet_count;
	}

	public void setBullet_count(int bullet_count) {
		this.bullet_count = bullet_count;
	}
	
	public void add_bullets(int num)
	{
		this.setBullet_count(this.getBullet_count()+num);
	}
	
	public void shoot(Player p)
	{
		if(this.getBullet_count()==0)
		{
			System.out.println("û�ӵ���");
			return;
		}
			this.setBullet_count(this.getBullet_count()-1);
			p.hurt(this);
		
	}
	
	
	

}
